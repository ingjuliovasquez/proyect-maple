<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    public $table = 'books';
    public $fillable = [
        'id',
        'title',
        'category_id',
        'author',
        'realease_date',
        'publish_date'

    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
