<?php

namespace App\Http\Controllers;

use App\Models\Book;

use Illuminate\Http\Request;

class BooksController extends Controller
{

    /**
     * Show index page
     */
    public function index()
    {
        $items = Book::with('category')->get();
        return response()->json([
            'status' => 200,
            'items' => $items
        ]);
    }

    public function show(Book $book)
    {
        $item = $book->with('category')->findOrFail($book->id);

        return $item ? response()->json(['book' => $item, 'status' => 200]) : response()->json([
            'status' => 404
        ]);
    }

    public function store(Request $request)
    {
        $book = Book::create([
            'title' => $request->title,
            'author' => $request->author,
            'category_id' => $request->category,
            'realease_date' => $request->realease_date,
            'publish_date' => $request->publish_date,
        ]);

        $book->load('category')->get();

        return $book ? response()->json([
            'status' => 200,
            'book' => $book
        ]) : response()->json([
            'status' => 400,
            'message' => 'Error al almacenar el registro'
        ]);
    }

    public function update(Request $request, Book $book)
    {

        $book->update([
            'title' => $request->title,
            'author' => $request->author,
            'category_id' => $request->category,
            'realease_date' => $request->realease_date,
            'publish_date' => $request->publish_date,
        ]);

        $book->load('category')->get();

        return $book ? response()->json([
            'status' => 200,
            'book' => $book
        ]) : response()->json([
            'status' => 400,
            'message' => 'Error al actualizar el registro'
        ]);
    }

    public function destroy(Book $book)
    {
        $deleted = $book->delete();
        return $deleted ? response()->json($deleted) : response()->json([
            'status' => 400,
            'message' => 'Error al eliminar el registro'
        ]);
    }
}
