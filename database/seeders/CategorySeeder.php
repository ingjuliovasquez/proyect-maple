<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Novela',
                'slug' => Str::slug('Novela'),
                'color' => '#06d6a0'
            ],
            [
                'name' => 'Progress',
                'slug' => Str::slug('Progress'),
                'color' => '#118ab2'
            ],
            [
                'name' => 'Tecnología',
                'slug' => Str::slug('Tecnología'),
                'color' => '#073b4c'
            ],
            [
                'name' => 'Comedia',
                'slug' => Str::slug('Comedia'),
                'color' => '#ffd166'
            ],
        ]);
    }
}
