<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'id' => 1,
                'title' => 'El Freno 2002',
                'category_id' => 1,
                'author' => 'Julio Vásquez',
                'realease_date' => '2022-01-01 12:01:00',
                'publish_date' => '2022-01-01 12:01:00'
            ],
            [
                'id' => 2,
                'title' => 'Building x2',
                'category_id' => 2,
                'author' => 'Ramiro Arthur',
                'realease_date' => '2022-01-01 12:01:00',
                'publish_date' => '2022-01-01 12:01:00'
            ],
            [
                'id' => 3,
                'title' => 'Computo en la nuve',
                'category_id' => 3,
                'author' => 'Ramiro Arthur',
                'realease_date' => '2022-01-01 12:01:00',
                'publish_date' => '2022-01-01 12:01:00'
            ],
            [
                'id' => 4,
                'title' => 'La historia entre nosotros',
                'category_id' => 1,
                'author' => 'Julio Vásquez',
                'realease_date' => '2022-01-01 12:01:00',
                'publish_date' => '2022-01-01 12:01:00'
            ],
        ]);
    }
}
