<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Book;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Book::class;

    public function definition()
    {
        return [
            //
        'title' => $this->faker->word(),
        'category_id' => $this->faker->randomElement([1, 2, 3, 4]),
        'author' => $this->faker->firstName(),
        'realease_date' => "2017-11-24 15:55:32",
        'publish_date' => "2017-11-24 15:55:32"
        ];

    }
}
