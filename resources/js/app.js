// require('./bootstrap');
import * as bootstrap from "bootstrap";
window.bootstrap = bootstrap;
import jquery from "jquery";
import Alpine from "alpinejs";
window.$ = jquery;
window.Alpine = Alpine;
Alpine.start();

window.headers = {
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
};

