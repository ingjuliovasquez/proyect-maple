<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ @csrf_token() }}">
    <title>Maple Books</title>
    <link rel="stylesheet" href="css/app.css">
</head>

<body>
    @include('components.navbar.navbar')
    <main>
        <div class="container mt-3">
            @include('components.modals.books-modal', ['categories' => $categories])
            @include('components.table.table')
        </div>
    </main>
    <script src="js/app.js"></script>
    <script type="module" src="js/getBooksData.mjs"></script>
    <script type="module" src="js/createBook.mjs"></script>
    <script type="module" src="js/editBook.mjs"></script>
    <script type="module" src="js/deleteBooK.mjs"></script>

</body>

</html>
