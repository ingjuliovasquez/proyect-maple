<table class="table table-hover table-border" id="table-books">
    <thead>
        <tr class="table-dark">
            <th scope="col">Titulo</th>
            <th scope="col">Categoría</th>
            <th class="text-center" scope="col">Autor</th>
            <th class="text-center" scope="col">Fecha de lanzamiento</th>
            <th class="text-center" scope="col">Fecha de publicación</th>
            <th class="text-center" scope="col">Opciones</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
