<!-- Button trigger modal -->
<div class="container d-flex justify-content-end my-4">
    <button type="button" class="btn btn-primary btn-modal-form" data-bs-target="#myModalForm">
        Agregar
    </button>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalForm" tabindex="-1" aria-labelledby="myModalFormLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="myModalFormLabel">Agregar nuevo libro</h1>
                <button type="button" class="btn-close" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="myForm" name="myForm" class="form-horizontal" novalidate="">
                    <div class="form-group mb-3">
                        <label>Título</label>
                        <input required type="text" class="form-control" id="title" name="title"
                            placeholder="Ingresa un título" value="">
                    </div>
                    <div class="form-group mb-3">
                        <label>Autor</label>
                        <input required type="text" class="form-control" id="author" name="author"
                            placeholder="Ingresa un autor" value="">
                    </div>
                    <div class="form-group mb-3">
                        <label>Categoría</label>
                        <select class="form-select" name="category" id="category">
                            <option selected disabled>------Seleccionar------</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="form-group mb-3">
                            <label>Fecha de publicación</label>
                            <input required type="date" class="form-control" name="publish_date" id="publish_date">
                        </div>
                        <div class="form-group mb-3">
                            <label>Fecha de lanzamiento</label>
                            <input required type="date" class="form-control" name="realease_date" id="realease_date">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-close-modal">Cerrar</button>
                <button type="button" class="btn btn-primary btn-submit" type='submit'>Guardar</button>
            </div>
        </div>
    </div>
</div>
