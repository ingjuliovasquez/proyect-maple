import getBooksData from "./getBooksData.mjs";

$(document).on("click", ".delete-book", function () {
    const id = $(this).val();

    $.ajax({
        type: "DELETE",
        url: `api/books/${id}`,
        headers: headers.headers,
        success: () => {
            $("#myForm")[0].reset();
            getBooksData();
        },
    });
});
