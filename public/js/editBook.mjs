import getBooksData from "./getBooksData.mjs";

const modal = new bootstrap.Modal("#myModalForm");

$(document).on("click", ".btn-close-modal", function () {
    modal.hide();
});

$(document).on("click", ".btn-close", function () {
    modal.hide();
});

$(document).on("click", ".edit-book", function () {
    const btnUpdate = $(".btn-submit");
    btnUpdate.removeClass("btn-submit");
    btnUpdate.addClass("btn-update");

    const id = $(this).val();
    $(".btn-update").val(id);

    $.ajax({
        type: "GET",
        url: `api/books/${id}`,
        success: ({ book }) => {
            $("#title").val(book.title);
            $("#author").val(book.author);
            $("#category").val(book.category?.id);
            $("#publish_date").val(book.publish_date);
            $("#realease_date").val(book.realease_date);
            modal.show();
        },
    });
});

$(document).ready(() => {
    $(document).on("click", ".btn-update", function (e) {
        e.preventDefault();

        const id = $(this).val();

        const formData = {
            title: $("#title").val(),
            author: $("#author").val(),
            category: $("#category").val(),
            publish_date: $("#publish_date").val(),
            realease_date: $("#realease_date").val(),
        };

        $.ajax({
            type: "PUT",
            url: `api/books/${id}`,
            data: formData,
            dataType: "json",
            headers: headers.headers,
            success: () => {
                modal.hide();
                $("#myForm")[0].reset();
                getBooksData();
            },
        });
    });
});
