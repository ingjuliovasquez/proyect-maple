import getBooksData from "./getBooksData.mjs";

const modal = new bootstrap.Modal("#myModalForm");

$(document).on("click", ".btn-close-modal", function () {
    modal.hide();
});

$(document).on("click", ".btn-close", function () {
    modal.hide();
});

$(document).on("click", ".btn-modal-form", function () {
    const btnCreate = $(".btn-update");
    btnCreate.removeClass("btn-update");
    btnCreate.addClass("btn-submit");
    modal.show();
});

$(document).ready(() => {
    const modal = new bootstrap.Modal("#myModalForm");
    $(document).on("click", ".btn-submit", function (e) {
        e.preventDefault();

        const formData = {
            title: $("#title").val(),
            author: $("#author").val(),
            category: $("#category").val(),
            publish_date: $("#publish_date").val(),
            realease_date: $("#realease_date").val(),
        };

        $.ajax({
            type: "POST",
            url: "api/books",
            data: formData,
            dataType: "json",
            headers: headers,
            success: () => {
                modal.hide();
                $("#myForm")[0].reset();
                getBooksData();
            },
        });
    });
});
