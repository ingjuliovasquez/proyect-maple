export const getBooksData = () => {
    $.ajax("api/books").done(({ status, items }) => {
        if (status === 200) {
            $("tbody").html("");
            items.map((item) => {
                $("tbody").append(`
                    <tr>
                        <td>${item.title}</td>
                        <td> <span class="badge" style="background: ${
                            item.category?.color
                        }">${item.category?.name}</span></td>
                        <td class="text-center">${item.author}</td>
                        <td class="text-center">${new Date(
                            item.realease_date
                        ).toLocaleDateString("es-Mx")}</td>
                        <td class="text-center">${new Date(
                            item.publish_date
                        ).toLocaleDateString("es-Mx")}</td>
                        <td class="text-center d-flex justify-content-evenly"> 
                            <button class="delete-book btn btn-danger bt-sm" value="${item.id}">
                                Borrar
                            </button>
                            <button class="edit-book btn btn-primary bt-sm" value="${item.id}">
                                Editar
                            </button>
                        </td>
                    </tr>
                `);
            });
        }
    });
};

$(document).ready(() => {
    getBooksData();
});

export default getBooksData;
